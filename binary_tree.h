#ifndef __BINARY_TREE__
#define __BINARY_TREE__

typedef struct node_t {
    char key;
    float number;
    struct node_t *right, *left;
} node_t;

// receive input, put on the tree and return the root
// void receive_tree (node_t *node);
node_t *receive_tree ();

// return the string to be insert
float transform_input (char *input, int *i);

// create a node and set his values
node_t *create_node (char value, float num);

// calculate the expressions
void calculate (node_t *node);

#endif