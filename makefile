CC = gcc
CFLAGS = -Wall

OBJ = binary_tree.o main.o
LIB = binary_tree.h
EXECUTABLE = main

all: $(EXECUTABLE)

# executable
main: $(OBJ)

# objects file
binary_tree.o: $(LIB) binary_tree.c
main.o:        $(LIB) main.c

# remove arquivos temporários
clean:
	-rm -f *~ *.o

# remove tudo o que não for o código fonte original
purge: clean
	-rm -f $(EXECUTABLE)