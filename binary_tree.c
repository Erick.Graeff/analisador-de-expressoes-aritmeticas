#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "binary_tree.h"

#define NODE_SIZE 10

// receive input and put on the tree
node_t *receive_tree (char *input, int *i) {
    node_t *node = NULL;

    if (input [*i] == '(') {
        (*i)++;
        // if the input is a operator,
        // alloc him
        if (!isdigit (input [*i])) {
            node = create_node (input [*i], 0);
            (*i)++;
        }
        // else alloc the number
        else
            node = create_node ('0', transform_input (input, i));

        node->left = receive_tree (input, i);
        node->right = receive_tree (input, i);
        (*i)++;
    }
    return node;
}

// return the number to be insert
float transform_input (char *input, int *i) {
    char new_number [NODE_SIZE + 1];
    int index = 0;

    while (input [*i] != '(' && input [*i] != ')' && input [*i] != '\0') {
        new_number [index] = input [*i];
        (*i)++;
        index++;
    }

    new_number [index] = '\0';
    return atof (new_number);
}

// create a node, set his values
// and return a pointer to him
node_t *create_node (char value, float num) {
    node_t *node = malloc (sizeof (node_t));

    // key receive the operator
    node->key = value;
    node->number = num;
    node->left = NULL;
    node->right = NULL;

    return node;
}

void calculate (node_t *node) {
    if (node) {
        calculate (node->left);
        calculate (node->right);

        // calculates the multiplication
        if (node->key == '*') {
            printf ("%.2f * %.2f\n", node->left->number, node->right->number);
            node->number = node->left->number * node->right->number;
            free (node->left);
            free (node->right);
        }

        // calculates the division
        else if (node->key == '/') {
            printf ("%.2f / %.2f\n", node->left->number, node->right->number);
            node->number = node->left->number / node->right->number;
            free (node->left);
            free (node->right);
        }

        // calculates the sum
        else if (node->key == '+') {
            printf ("%.2f + %.2f\n", node->left->number, node->right->number);
            node->number = node->left->number + node->right->number;
            free (node->left);
            free (node->right);
        }

        // calculates the subtraction
        else if (node->key == '-') {
            printf ("%.2f - %.2f\n", node->left->number, node->right->number);
            node->number = node->left->number - node->right->number;
            free (node->left);
            free (node->right);
        }
    }
}