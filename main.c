#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "binary_tree.h"

#define INPUT 100

int main () {
    int i = 0;
    char input [INPUT + 1];
    scanf ("%s", input);

    node_t *root = receive_tree (input, &i);

    calculate (root);

    // display the final value on the root and delete root
    printf ("%.2f\n", root->number);
    free (root);

    return 0;
}